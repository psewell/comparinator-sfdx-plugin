comparinator
============

I&#39;ll be back

[![Version](https://img.shields.io/npm/v/comparinator.svg)](https://npmjs.org/package/comparinator)
[![CircleCI](https://circleci.com/gh/sfdxplugins/comparinator/tree/master.svg?style=shield)](https://circleci.com/gh/sfdxplugins/comparinator/tree/master)
[![Appveyor CI](https://ci.appveyor.com/api/projects/status/github/sfdxplugins/comparinator?branch=master&svg=true)](https://ci.appveyor.com/project/heroku/comparinator/branch/master)
[![Codecov](https://codecov.io/gh/sfdxplugins/comparinator/branch/master/graph/badge.svg)](https://codecov.io/gh/sfdxplugins/comparinator)
[![Greenkeeper](https://badges.greenkeeper.io/sfdxplugins/comparinator.svg)](https://greenkeeper.io/)
[![Known Vulnerabilities](https://snyk.io/test/github/sfdxplugins/comparinator/badge.svg)](https://snyk.io/test/github/sfdxplugins/comparinator)
[![Downloads/week](https://img.shields.io/npm/dw/comparinator.svg)](https://npmjs.org/package/comparinator)
[![License](https://img.shields.io/npm/l/comparinator.svg)](https://github.com/sfdxplugins/comparinator/blob/master/package.json)

<!-- toc -->
* [Debugging your plugin](#debugging-your-plugin)
<!-- tocstop -->
<!-- install -->
<!-- usage -->
```sh-session
$ npm install -g sfdx-plugin-comparinator
$ sfdx COMMAND
running command...
$ sfdx (-v|--version|version)
sfdx-plugin-comparinator/0.0.3 linux-x64 node-v12.18.0
$ sfdx --help [COMMAND]
USAGE
  $ sfdx COMMAND
...
```
<!-- usagestop -->
<!-- commands -->
* [`sfdx comparinator:perms:compare -s <array> -t <array> [-g] [-d] [-f <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-comparinatorpermscompare--s-array--t-array--g--d--f-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx comparinator:perms:make -o [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-comparinatorpermsmake--o--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx comparinator:perms:merge -p <array> [-l] [-g] [-d] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-comparinatorpermsmerge--p-array--l--g--d--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx comparinator:perms:compare -s <array> -t <array> [-g] [-d] [-f <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Compares aggregate permisisons from source list against and target list writing higher permissions contained in target list to stdout

```
USAGE
  $ sfdx comparinator:perms:compare -s <array> -t <array> [-g] [-d] [-f <string>] [-u <string>] [--apiversion <string>] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -d, --getpermsdir                                                                 look for permission sets in the sfdx
                                                                                    project permissions folder

  -f, --tofile=tofile                                                               write differences to file in xml
                                                                                    format

  -g, --getorg                                                                      look for permission sets on
                                                                                    Salesforce org

  -s, --sourceperms=sourceperms                                                     (required) Source list of permission
                                                                                    set files to be compared against
                                                                                    target (use permission set names
                                                                                    when using -g or -d); list is comma
                                                                                    separated

  -t, --targetperms=targetperms                                                     (required) Target list of permission
                                                                                    set files to be compared against
                                                                                    target (use permission set names
                                                                                    when using -g or -d); list is comma
                                                                                    separated

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx comparinator:perms:compare -s permset1.xml,permset2.xml -t permset3.xml,permset4.xml -f differences.xml
    
  $ sfdx comparinator:perms:compare -s permset1 -t permset2,permset3 -g
```

## `sfdx comparinator:perms:make -o [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Makes a permission set file containing permissions as set by other flags

```
USAGE
  $ sfdx comparinator:perms:make -o [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -o, --makeobjects                                                                 (required) grant read only access to
                                                                                    all custom objects in org

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx comparinator:perms:make -o 
    
  $ sfdx comparinator:perms:make -o --json
```

## `sfdx comparinator:perms:merge -p <array> [-l] [-g] [-d] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Merges permission sets and writes to stdout

```
USAGE
  $ sfdx comparinator:perms:merge -p <array> [-l] [-g] [-d] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -d, --getpermsdir                                                                 look for permission sets in the sfdx
                                                                                    project permissions folder

  -g, --getorg                                                                      look for permission sets on
                                                                                    Salesforce org

  -l, --formatasprofile                                                             format output as profile metadata
                                                                                    (default is permission set)

  -p, --permissionsets=permissionsets                                               (required) list of permission set
                                                                                    files to merge (or names when using
                                                                                    -g or -d); list is comma separated

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx comparinator:perms:merge -p permset1.xml,permset2.xml,permset3.xml
    
  $ sfdx comparinator:perms:merge -p permset1,permset2,permset3 -g
```
<!-- commandsstop -->
<!-- debugging-your-plugin -->
# Debugging your plugin
We recommend using the Visual Studio Code (VS Code) IDE for your plugin development. Included in the `.vscode` directory of this plugin is a `launch.json` config file, which allows you to attach a debugger to the node process when running your commands.

To debug the `hello:org` command: 
1. Start the inspector
  
If you linked your plugin to the sfdx cli, call your command with the `dev-suspend` switch: 
```sh-session
$ sfdx hello:org -u myOrg@example.com --dev-suspend
```
  
Alternatively, to call your command using the `bin/run` script, set the `NODE_OPTIONS` environment variable to `--inspect-brk` when starting the debugger:
```sh-session
$ NODE_OPTIONS=--inspect-brk bin/run hello:org -u myOrg@example.com
```

2. Set some breakpoints in your command code
3. Click on the Debug icon in the Activity Bar on the side of VS Code to open up the Debug view.
4. In the upper left hand corner of VS Code, verify that the "Attach to Remote" launch configuration has been chosen.
5. Hit the green play button to the left of the "Attach to Remote" launch configuration window. The debugger should now be suspended on the first line of the program. 
6. Hit the green play button at the top middle of VS Code (this play button will be to the right of the play button that you clicked in step #5).
<br><img src=".images/vscodeScreenshot.png" width="480" height="278"><br>
Congrats, you are debugging!
